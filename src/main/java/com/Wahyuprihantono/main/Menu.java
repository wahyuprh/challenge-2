package com.Wahyuprihantono.main;

import com.Wahyuprihantono.OOP.mainMenu;

import java.util.Scanner;

public class Menu implements mainMenu{

    Scanner scann = new Scanner(System.in);
    String csvPath = "src/Data/data_sekolah.csv";
    String SimpanMeMeModPath = "src/Data/outputMeMoMod.txt";
    String SimpanModesekolah = "src/Data/outputModeSekolah.txt";
    MeMeMod m3 = new MeMeMod();

    public void switchMenu(){

        switch (MainMenu()){
            case 1:
                m3.write(SimpanMeMeModPath);
                break;
            case 2:
                m3.writeMode(SimpanModesekolah);
                break;
            case 3:
                m3.write(SimpanMeMeModPath);
                m3.writeMode(SimpanModesekolah);
                break;
            case 0:
                System.out.println("Keluar YA GES YA");
                System.exit(0);
            default:
                System.out.println("\n");
                System.out.println("PILIH ANGKA YANG BENAR!!!");
                System.out.println("PILIH ANGKA YANG BENAR!!!");
                System.out.println("PILIH ANGKA YANG BENAR!!!");
                System.out.println("PILIH ANGKA YANG BENAR!!!");
                System.out.println("PILIH ANGKA YANG BENAR!!!");
                System.out.println("\n");
                switchMenu();
                break;
        }
    }
    public int MainMenu() {
        System.out.println("=================================================");
        System.out.println("Aplikasi Mengolah Data Nilai Siswa");
        System.out.println("-------------------------------------------------");
        System.out.println("1. Menghitung Mean-Median-Modus");
        System.out.println("2. Menghitung Modus Sekolah");
        System.out.println("3. Jalankan dua program diatas");
        System.out.println("0. Keluar YA GES YA");
        System.out.println("=================================================");

        System.out.print("Masukkan Pilihan: ");
        int pilih = scann.nextInt();
        return pilih;
    }
}
