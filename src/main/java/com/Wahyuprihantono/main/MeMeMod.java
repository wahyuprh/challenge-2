package com.Wahyuprihantono.main;

import java.io.*;
import java.util.*;

public class MeMeMod {

    public List<Integer> read (String path) {
        try {
            File file = new File(path);
            FileReader reader = new FileReader(file);
            BufferedReader br = new BufferedReader(reader);

            String line = " ";
            String[] tempArr;

            List<Integer> lisInt = new ArrayList<>();
            while ((line = br.readLine()) != null) {
                tempArr = line.split(";");

                for (int i = 0; i < tempArr.length; i++) {
                    if (i == 0) {

                } else{
                    String temp = tempArr[i];
                    int tempInt = Integer.parseInt(temp);
                    lisInt.add(tempInt);
                }
            }
            }reader.close();
            return lisInt;
        }catch(Exception e){
        e.printStackTrace();
        }
        return null;
    }
    public void write(String simpanDisini){
        Menu menu = new Menu();

        read(menu.csvPath);
        try {
            File file = new File(simpanDisini);
            if (file.createNewFile()) {
                System.out.println("File telah disimpan di " + simpanDisini);
            }
            FileWriter fw = new FileWriter(file);
            BufferedWriter bw = new BufferedWriter(fw);

            bw.write("Berikut data yang terkumpul : ");
            bw.write("\n");

            //Mean
            bw.write("Mean : " + mean(read(menu.csvPath)));
            bw.write("\n");

            //Median
            bw.write("Median : " + median(read(menu.csvPath)));
            bw.write("\n");

            //Mode
            bw.write("Modus : " + mode(read(menu.csvPath)));

            bw.flush();
            bw.close();

        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public void writeMode(String simpanMod){
        Menu menu = new Menu();

        try{
            File file = new File(simpanMod);
            if(file.createNewFile()){
                System.out.println("Data Modus ada di : " + simpanMod);
            }
            FileWriter fw = new FileWriter(file);
            BufferedWriter bw = new BufferedWriter(fw);
            Map<Integer, Integer> hMap = freq(read(menu.csvPath));
            Set<Integer> key = hMap.keySet();

            bw.write("Ini hasil :");
            bw.write("\n");
            bw.write("Nilai :\t\t\t" +"|\t\t" + "Frekuensi" + "\n");

            for(Integer nilai : key){
                bw.write(nilai + "\t\t\t\t" + "|\t\t" + hMap.get(nilai) + "\n");
            }

            bw.flush();
            bw.close();
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private double mean(List<Integer>list){
        IntSummaryStatistics stats = list.stream()
                .mapToInt(Integer::intValue)
                .summaryStatistics();
        return stats.getAverage();
    }
    private double median(List<Integer> list) {
        Arrays.sort(new List[]{list});
        double median;
        if (list.size() % 2 == 0)
            median = ((double) list.get(list.size() / 2) + (double) list.get(list.size() / 2 - 1)) / 2;
        else
            median = (double) list.get(list.size() / 2);
        return median;
    }
    private int mode(List<Integer> list) {
        HashMap<Integer, Integer> hm = new HashMap<>();
        int max = 1;
        int temp = 0;

        for (Integer integer : list) {

            if (hm.get(integer) != null) {

                int count = hm.get(integer);
                count++;
                hm.put(integer, count);

                if (count > max) {
                    max = count;
                    temp = integer;
                }
            } else
                hm.put(integer, 1);
        }
        return temp;
    }
    private Map<Integer, Integer> freq(List<Integer> array) {
        Set<Integer> distinct = new HashSet<>(array);
        Map<Integer, Integer> mMap = new HashMap<>();

        for (Integer s : distinct) {
            mMap.put(s, Collections.frequency(array, s));
        }
        return mMap;
    }
}